package sanpham.mvc;

import java.sql.SQLException;

public class DTController {
	private DTModel model;

	public DTController() {
		model = new DTModel();
	}

	public int insertDT(DienThoai dt) {
		int cnt = 0;
		try {
			cnt = model.insert(dt);
		} catch (SQLException ex) {
			System.out.println("Error: " + ex.toString());
		}
		return cnt;
	}

	public DienThoai displayDienThoai(DienThoai dt2) {
		DienThoai dt = null;
		try {
			dt = model.display(dt2);
		} catch (SQLException ex) {
			System.out.println("Error: " + ex.toString());
		}
		return dt;
	}
	
	public int updatetDT(DienThoai dt3) {
		int cnt = 0;
		try {
			cnt = model.update(dt3);
		} catch (SQLException ex) {
			System.out.println("Error: " + ex.toString());
		}
		return cnt;
	}
}
