package sanpham.mvc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DTModel {

	private static Connection getConnection() throws Exception {
		Class.forName("com.mysql.cj.jdbc.Driver");
		return DriverManager.getConnection("jdbc:mysql://localhost:3307/sanphamdb", "root", "Tuan08072002@");
	}

	public int insert(DienThoai dt) throws SQLException {
		Connection connection = null;
		PreparedStatement ps = null;
		int cnt = 0;
		try {
			connection = DTModel.getConnection();
			ps = connection.prepareStatement("insert into dienthoai values(?,?,?,?,?)");
			int i = 0;
			ps.setInt(++i, dt.getId());
			ps.setNString(++i, dt.getTen());
			ps.setNString(++i, dt.getHang());
			ps.setInt(++i, dt.getGia());
			ps.setInt(++i, dt.getSoluong());
			cnt = ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (ps != null) {
				ps.close();
			}
			if (connection != null) {
				connection.close();
			}
		}
		return cnt;
	}

	public DienThoai display(DienThoai dt2) throws SQLException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		DienThoai dt = null;
		int cnt = 0;
		try {
			con = DTModel.getConnection();
			ps = con.prepareStatement("select * from dienthoai where id = ?");
			ps.setInt(1, dt2.getId());
			rs = ps.executeQuery();
			dt = new DienThoai();
			while (rs.next()) {
				dt.setId(rs.getInt("id"));
				dt.setTen(rs.getString("ten"));
				dt.setHang(rs.getString("hang"));
				dt.setGia(rs.getInt("gia"));
				dt.setSoluong(rs.getInt("soluong"));
				cnt++;
			}
			if (cnt > 0)
				return dt;
		} catch (Exception e) {
			System.out.println(e);
		} finally {
			if (rs != null) {
				rs.close();
			}
			if (ps != null) {
				ps.close();
			}
			if (con != null) {
				con.close();
			}
		}
		return null;
	}

	public int update(DienThoai dt3) throws SQLException {
		Connection connection = null;
		PreparedStatement ps = null;
		int cnt = 0;
		try {
			connection = DTModel.getConnection();
			ps = connection.prepareStatement("update dienthoai set ten=?,hang=,gia=?,soluong=? WHERE id=?");
//			int i = 0;
			ps.setNString(2, dt3.getTen());
			ps.setNString(3, dt3.getHang());
			ps.setInt(4, dt3.getGia());
			ps.setInt(5, dt3.getSoluong());
			ps.setInt(1, dt3.getId());
			cnt = ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (ps != null) {
				ps.close();
			}
			if (connection != null) {
				connection.close();
			}
		}
		return cnt;
	}

}
