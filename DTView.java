package sanpham.mvc;

import java.util.Scanner;

public class DTView {
	private DienThoai dt;
	private DTController controller;
	Scanner sc;

	public DTView(DienThoai dt, DTController controller, Scanner sc) {
		super();
		this.dt = dt;
		this.controller = controller;
		this.sc = sc;
	}

	public void insert() {
		System.out.println("Nhap id: ");
		dt.setId(sc.nextInt());
		sc.nextLine();
		System.out.println("Nhap ten dienthoai: ");
		dt.setTen(sc.nextLine());
		System.out.println("Nhap ten hang: ");
		dt.setHang(sc.nextLine());
		System.out.println("Nhap gia: ");
		dt.setGia(sc.nextInt());
		System.out.println("Nhap soluong: ");
		dt.setSoluong(sc.nextInt());
		int cnt = controller.insertDT(dt);
		if (cnt != 0) {
			System.out.println("Insert successfully!");
		} else {
			System.out.println("Cannot insert!");
		}
	}

	public void print() {
		System.out.println("Enter empolyee's id to search: ");
		int id = sc.nextInt();
		dt.setId(id);
		DienThoai phone = controller.displayDienThoai(dt);
		if (phone != null) {
			System.out.println("ID: "+phone.getId());
			System.out.println("ten: "+phone.getTen());
			System.out.println("hang: "+phone.getHang());
			System.out.println("gia: "+phone.getGia());
			System.out.println("soluong: "+phone.getSoluong());

		} else {
			System.out.println("Not found!");
		}
	}

	public void update() {
		System.out.println("Nhap id: ");
		dt.setId(sc.nextInt());
		sc.nextLine();
		System.out.println("Nhap ten dienthoai: ");
		dt.setTen(sc.nextLine());
		System.out.println("Nhap ten hang: ");
		dt.setHang(sc.nextLine());
		System.out.println("Nhap gia: ");
		dt.setGia(sc.nextInt());
		System.out.println("Nhap soluong: ");
		dt.setSoluong(sc.nextInt());

		int cnt = controller.updatetDT(dt);
		if (cnt != 0) {
			System.out.println("Upadte successfully!");
		} else {
			System.out.println("Cannot update!");
		}
	}
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int chon;
		DienThoai dt = new DienThoai();
		DTController controller = new DTController();
		DTView view = new DTView(dt, controller, sc);
		do {
			System.out.println("1.Create phone");
			System.out.println("2.Display phone base on id");
			System.out.println("3.Update phone");
			System.out.println("4.Exit");
			System.out.print("Please enter the choice: ");
			chon = sc.nextInt();
			sc.nextLine();
			switch (chon) {
			case 1:
				view.insert();
				break;
			case 2:
				view.print();
				break;
			case 3:
				view.update();
				break;
			}
		} while (chon != 4);
	}
}
